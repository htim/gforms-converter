package com.gformsconverter.model;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by htim on 17.01.15.
 */
public class SurveyItem {

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    private Integer number;
    private String question;
    private List<String> answers;
    private Type type;

    @Override
    public String toString() {
        if (this.answers==null)
            return this.number + " " + this.question + " " + this.type;
        else
            return this.number + " " + this.question + " " + this.answers.stream().collect(Collectors.joining(";")) + " " + this.type;
    }

    public enum Type {
        Open,
        Radio,
        Check,
        Table
    }
}
