package com.gformsconverter;

import java.io.IOException;
import java.util.Arrays;

import com.gformsconverter.parser.Parser;
import com.gformsconverter.parser.ParserImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
/**
 * Created by htim on 17.01.15.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Appication {

    public static void main(String[] args) {
        SpringApplication.run(Appication.class,args);
        }
    }

