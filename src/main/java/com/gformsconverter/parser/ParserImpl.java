package com.gformsconverter.parser;


import com.gformsconverter.model.SurveyItem;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

import org.codehaus.jackson.map.ObjectMapper;


/**
 * Created by htim on 17.01.15.
 */
public class ParserImpl implements Parser {
    @Override
    public String Parse(String URL) throws IOException {

        List<SurveyItem> surveyItems = new ArrayList<>();

        Document doc = Jsoup.connect(URL).get();
        Elements items = doc.select("div.ss-form-question");
        for (Element item : items) {
            Elements subitems = item.select("div.ss-item.ss-text");
            setOpenQuestion(surveyItems, subitems);
            subitems.clear();
            subitems = item.select("div.ss-item.ss-checkbox");
            setCheckboxQuestion(surveyItems,subitems);
            subitems.clear();
            subitems = item.select("div.ss-item.ss-radio");
            setRadioQuestion(surveyItems,subitems);
            subitems.clear();
            subitems = item.select("div.ss-item.ss-grid");
            setTableQuestion(surveyItems,subitems);
            subitems.clear();
            subitems = item.select("div.ss-item.ss-paragraph-text");
            setOpenParagraphText(surveyItems,subitems);
            subitems.clear();
            }

        ObjectMapper mapper = new ObjectMapper();
        String result = mapper.writeValueAsString(surveyItems);
        return result;
    }

    private void setOpenQuestion(List<SurveyItem> surveyItems, Elements subitems) {
        for (Element subitem : subitems) {
            SurveyItem surveyItem = new SurveyItem();
            surveyItem.setType(SurveyItem.Type.Open);
            String node = subitem.select("div.ss-form-entry").select("input").attr("aria-label");
            String[] splited = node.split("\\.");
            surveyItem.setQuestion(splited[1]);
            surveyItem.setNumber(Integer.parseInt(splited[0]));
            surveyItems.add(surveyItem);
        }
    }

    private void setCheckboxQuestion(List<SurveyItem> surveyItems, Elements subitems) {
        for (Element subitem : subitems) {
            SurveyItem surveyItem = new SurveyItem();
            surveyItem.setType(SurveyItem.Type.Check);
            String questionAndNumber = subitem.select("div.ss-form-entry").select("ul").attr("aria-label");
            String number = questionAndNumber.split("\\.")[0].trim();
            String question = questionAndNumber.split("\\.")[1].trim();
            surveyItem.setQuestion(question);
            surveyItem.setNumber(Integer.parseInt(number));
            List<String> answerList = new ArrayList<>();
            Elements answers = subitem.select("div.ss-form-entry").select("ul");
            for (Element item : answers) {
                String answer = item.select("li").select("label").select("span.ss-choice-label").html();
                String[] splitted = answer.split("\\r?\\n");
                Arrays.asList(splitted).stream().forEach(answerList::add);
            }
            surveyItem.setAnswers(answerList);
            surveyItems.add(surveyItem);
        }
    }

    private void setRadioQuestion(List<SurveyItem> surveyItems, Elements subitems) {
        for (Element subitem : subitems) {
            SurveyItem surveyItem = new SurveyItem();
            surveyItem.setType(SurveyItem.Type.Radio);
            String questionAndNumber = subitem
                    .select("div.ss-form-entry")
                    .select("ul")
                    .attr("aria-label");
            String number = questionAndNumber.split("\\.")[0].trim();
            String question = questionAndNumber.split("\\.")[1].trim();
            surveyItem.setQuestion(question);
            surveyItem.setNumber(Integer.parseInt(number));
            List<String> answerList = new ArrayList<>();
            Elements answers = subitem
                    .select("div.ss-form-entry")
                    .select("ul");
            for (Element item : answers) {
                String answer = item
                        .select("li")
                        .select("label")
                        .select("span.ss-choice-label")
                        .html();
                String[] splitted = answer.split("\\r?\\n");
                Arrays.asList(splitted).stream().forEach(answerList::add);
            }
            surveyItem.setAnswers(answerList);
            surveyItems.add(surveyItem);
        }
    }

    private void setTableQuestion(List<SurveyItem> surveyItems, Elements subitems) {
        for (Element subitem: subitems) {


            List<String> answers = new ArrayList<>();
            List<String> questions = new ArrayList<>();

            String questionAndNumber = subitem
                    .select("div.ss-form-entry")
                    .select("label.ss-q-item-label")
                    .select("div.ss-q-title")
                    .html();
            int a = 0;
            String mainQuestion = questionAndNumber.split("\\.")[1].trim();
            String number = questionAndNumber.split("\\.")[0];

            Elements thead = subitem
                    .select("div.ss-form-entry")
                    .select("div")
                    .select("table")
                    .select("thead")
                    .select("tr")
                    .select("td.ss-gridnumbers");

            for (Element head : thead) {
                String answer = head.getElementsByTag("label").html();
                if (!answer.isEmpty())
                    answers.add(answer);
            }
            Elements tbody = subitem
                    .select("div.ss-form-entry")
                    .select("div")
                    .select("table")
                    .select("tbody")
                    .select("tr");
            for (Element body : tbody) {
                String question = body.attr("aria-label");
                questions.add(question);
            }

            for (String question : questions) {
                SurveyItem surveyItem = new SurveyItem();
                surveyItem.setType(SurveyItem.Type.Table);
                surveyItem.setAnswers(answers);
                surveyItem.setQuestion(new StringBuilder().append(mainQuestion).append(" [").append(question).append("]").toString());
                surveyItem.setNumber(Integer.parseInt(number));
                surveyItems.add(surveyItem);
            }
        }
    }

    private void setOpenParagraphText(List<SurveyItem> surveyItems, Elements subitems) {
        for (Element subitem : subitems) {
            SurveyItem surveyItem = new SurveyItem();
            surveyItem.setType(SurveyItem.Type.Open);
            String node = subitem.select("div.ss-form-entry").select("textarea").attr("aria-label");
            String[] splited = node.split("\\.");
            surveyItem.setQuestion(splited[1]);
            surveyItem.setNumber(Integer.parseInt(splited[0]));
            surveyItems.add(surveyItem);
        }
    }


}
