package com.gformsconverter.parser;

import java.io.IOException;

/**
 * Created by htim on 17.01.15.
 */
public interface Parser {
    String Parse(String URL) throws IOException;
}
