package com.gformsconverter.controllers;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import com.gformsconverter.parser.*;
/**
 * Created by htim on 17.01.15.
 */
@Controller
public class ParseController {

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("message","Hello!");
        return "index";
    }
}
